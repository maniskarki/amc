package np.com.sagartamang.amc.main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothSocket;
import android.companion.BluetoothDeviceFilter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final int FETCH_DEVICE = 1;
    private final String TAG = "AMC";

    // Handler to send and receive message from threads
    private static Handler mHandler;

    private static byte[] COMMAND_ENGINE_START = "a".getBytes();
    private static byte[] COMMAND_ENGINE_STOP = "b".getBytes();
    private static byte[] COMMAND_IGNITION_ON = "c".getBytes();
    private static byte[] COMMAND_IGNITION_OFF = "d".getBytes();

     private final int CONNECTION_ERROR = 2;

     private boolean isConnected = false;


    // Get default bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice mDevice;
    private ImageView ibEngine, ibIgnition;
    private Button btnForgotPassword, btnConnectToDevice, btnSettings;

    private ConnectThread connectThread;

    private int engineImage = R.drawable.start_engine;
    private int ignitionImage = R.drawable.ignition_on;


    // The BroadReceiver that listenes for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)){
                isConnected = true;
                ibEngine.setImageResource(R.drawable.start_engine);
                ibIgnition.setImageResource(R.drawable.ignition_on);
                btnConnectToDevice.setText("Disconnect");
                Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                isConnected = false;
                ibEngine.setImageResource(R.drawable.connect);
                ibIgnition.setImageResource(R.drawable.ignition_disabled);
                btnConnectToDevice.setText("Connect To Device");
                Toast.makeText(getApplicationContext(), "Disconnected", Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Show dialog if BluetoothAdapter is null
        if (mBluetoothAdapter == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You device doesn't support bluetooth");
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.show();
        } else {
            // Goto the BluetoothOff Activity if the bluetooth is OFF
            if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                Intent intent = new Intent(this, BluetoothOffActivity.class);
                startActivity(intent);
            }

            // If the bluetooth is on, initialize things
            init();
            initBluetoothConnectionListener();
            initHandler();
            autoConnect();
        }
    }

    private void autoConnect() {
        // Get the shared preferences
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        // Get the bluetooth name
        String bluetoothName = sharedPref.getString("device_name", "");
        String bluetoothAddress = sharedPref.getString("device_address", "");

        if (!"".equals(bluetoothAddress)){
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bluetoothAddress);
            connectThread = new ConnectThread(mHandler, device);
            connectThread.start();
        }
    }

    private void initBluetoothConnectionListener() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        connectThread.cancel();
        connectThread.stop();
        connectThread = null;

        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what){
                    case CONNECTION_ERROR:
                        Toast.makeText(getApplicationContext(), "Unable to connect", Toast.LENGTH_SHORT).show();
                        btnConnectToDevice.setText("Connect To Device");
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    private void init() {
        ibEngine = (ImageView) findViewById(R.id.ibEngine);
        ibEngine.setOnClickListener(this);

        ibIgnition = (ImageView) findViewById(R.id.ibIgnition);
        ibIgnition.setOnClickListener(this);

        btnForgotPassword = (Button) findViewById(R.id.btnForgotPassowrd);
        btnForgotPassword.setOnClickListener(this);

        btnConnectToDevice = (Button) findViewById(R.id.btnConnect);
        btnConnectToDevice.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == FETCH_DEVICE){
            if (resultCode ==  RESULT_OK){
                Bundle bundle = data.getExtras();
                final String deviceName = bundle.getString("device_name");
                final String deviceAddress = bundle.getString("device_address");
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(deviceAddress);

                SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
                String savedDeviceAddress = sharedPreferences.getString("device_address", "");
                if (!savedDeviceAddress.equals(deviceAddress)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Do you want to remember the device?");
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            saveBluetoothDataToSharePreferences(deviceName, deviceAddress);
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //
                        }
                    });
                    builder.show();
                }

                connectThread = new ConnectThread(mHandler, device);
                connectThread.start();

            }
        }
    }

    private void saveBluetoothDataToSharePreferences(String deviceName, String deviceAddress) {
        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("device_name", deviceName);
        editor.putString("device_address", deviceAddress);
        editor.commit();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == ibEngine.getId()) {
            if (!isConnected) {
                Intent intent = new Intent(this, ListDevicesActivity.class);
                startActivityForResult(intent, FETCH_DEVICE);
            } else {

                if (engineImage == R.drawable.stop_engine) {
                    engineImage = R.drawable.start_engine;
                    connectThread.write(COMMAND_ENGINE_START);
                } else {
                    engineImage = R.drawable.stop_engine;
                    connectThread.write(COMMAND_ENGINE_STOP);
                }
                ibEngine.setImageResource(engineImage);
            }

        } else if (v.getId() == ibIgnition.getId()) {
            if (isConnected) {
                if (ignitionImage == R.drawable.ignition_off) {
                    ignitionImage = R.drawable.ignition_on;
                    connectThread.write(COMMAND_IGNITION_ON);
                } else {
                    ignitionImage = R.drawable.ignition_off;
                    connectThread.write(COMMAND_IGNITION_OFF);
                }
                ibIgnition.setImageResource(ignitionImage);
            } else {
                Toast.makeText(getApplicationContext(), "First connect to a device !!!", Toast.LENGTH_SHORT).show();
            }

        } else if (v.getId() == btnForgotPassword.getId()) {
            Intent intent = new Intent(this, ForgotPasswordActivity.class);
            startActivity(intent);
        } else if (v.getId() == btnConnectToDevice.getId()){
            if (!isConnected) {
                Intent intent = new Intent(this, ListDevicesActivity.class);
                startActivityForResult(intent, FETCH_DEVICE);
            } else {
                connectThread.cancel();
            }

        }
    }

    // ConnectThread
    private class ConnectThread extends Thread{
        private final Handler mHandler;
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private MyBluetoothService myBluetoothService;

        public ConnectThread(Handler handler, BluetoothDevice device){
            mHandler = handler;

            // Use a temporary object hat is latter assigned to mmSocket
            // because mmSocket ins final.
            BluetoothSocket tmp = null;
            mmDevice = device;
            myBluetoothService = null;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevie.
                // MY_UUID is the app's UUI string, alose used in the
                ParcelUuid[] parcelUuids = mmDevice.getUuids();
                tmp = device.createRfcommSocketToServiceRecord(parcelUuids[0].getUuid());
            } catch (IOException e){
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run(){
            // Cancel discovery because it otherwise slows down the connection
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device throught the socket. This call
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException){
                // Unable to connect; close the socket and return
                Message msg = mHandler.obtainMessage(CONNECTION_ERROR);
                msg.sendToTarget();
                try {
                    mmSocket.close();
                } catch (IOException closeException){
                    Log.e(TAG, "Couldn't close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel(){
            try {
                mmSocket.close();
            } catch (IOException e){
                Message msg = mHandler.obtainMessage(CONNECTION_ERROR);
                msg.sendToTarget();
                Log.e(TAG, "Couldn't close the client socket", e);
            }
        }

        private void manageMyConnectedSocket(BluetoothSocket mmSocket) {
            myBluetoothService = new MyBluetoothService(mHandler, mmSocket);
        }

        public void write(byte[] bytes){
            myBluetoothService.write(bytes);
        }
    }

}
