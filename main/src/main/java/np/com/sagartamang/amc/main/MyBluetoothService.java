package np.com.sagartamang.amc.main;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

// BluetoothSerive after connection
public class MyBluetoothService {
    private static final String TAG = "AMC";
    private Handler mHandler;
    private ConnectedThread connectedThread;

    // Define several constants used when transmitting messages
    // service and the UI
    public interface MessageConstants {
        public static final int MESSAGE_READ = 0;
        public static final int MESSAGE_WRITE = 1;
        public static final int MESSAGE_TOAST = 2;
    }

    public MyBluetoothService(Handler handler, BluetoothSocket socket){
        mHandler = handler;
        connectedThread = new ConnectedThread(socket);
        connectedThread.start();
    }

    public void write(byte[] bytes){
        connectedThread.write(bytes);
    }

    public void cancel(){
        connectedThread.cancel();
        connectedThread.stop();
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; //mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket){
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp object because
            // member streams are final.

            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }

            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e){
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public  void run(){
            mmBuffer = new byte[1];
            int numBytes;

            // Keep listening to the InputStream until an exception occurs
            while(true){
                try {
                    // Read from the InputStream
                    numBytes = mmInStream.read(mmBuffer);

                    // Send the obtained bytes to the UI activity.
                    Message readMsg = mHandler.obtainMessage(MessageConstants.MESSAGE_READ, numBytes, -1, mmBuffer);
                    readMsg.sendToTarget();
                } catch (IOException e){
                    Log.d(TAG, "Input Stream was disconnected", e);
                    break;
                }
            }
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes){
            try {
                mmOutStream.write(bytes);

                // Share the sent message with the UI acitivity.
                Message writtenMsg = mHandler.obtainMessage(MessageConstants.MESSAGE_WRITE, -1, -1, mmBuffer);
                writtenMsg.sendToTarget();
            } catch(IOException e){
                Log.e(TAG, "Error occured when sending data",e);

                // Send a failure message back to the activity.
                Message writeErrorMsg = mHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString("toast", "Couldn't send data to the other device");
                writeErrorMsg.sendToTarget();
                mHandler.sendMessage(writeErrorMsg);
            }
        }

        // Call this method from the main activity to shutdown the connection.
        public void cancel(){
            try{
                mmSocket.close();
            } catch (IOException e){
                Log.e(TAG, "Could not close the connect socket",e);
            }
        }
    }
}
