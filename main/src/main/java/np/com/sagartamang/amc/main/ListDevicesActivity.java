package np.com.sagartamang.amc.main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ListDevicesActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tvPairedDevices, tvAvailableDevices;
    private ListView lvPairedDevices, lvAvailableDevices;
    private Button btnSearch;
    private boolean isScanning = false;

    static final int ENABLE_BLUETOOTH = 0;

    private ArrayList<BluetoothDevice> pairedDevicesList = new ArrayList<BluetoothDevice>();
    private ArrayList<BluetoothDevice> availableDevicesList = new ArrayList<BluetoothDevice>();

    // ListView Adapters
    CustomAvailableDevicesAdapter customAvailableDevicesAdapter = new CustomAvailableDevicesAdapter();
    CustomPairedDevicesAdapter customPairedDevicesAdapter = new CustomPairedDevicesAdapter();

    // Get default bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)){
                // Discovery has found a device. Get the BluetoothDevice object
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the discovered BluetoothDevice object to the availableDeviceList
                availableDevicesList.add(device);
                // Notify the adapter the dataset has changed so that the corresponding listview
                // can update its view
                customAvailableDevicesAdapter.notifyDataSetChanged();
            }
        }
    };

    //Create a BroadcastReceiver for ACTION_BOND_STATE_CHANGED
    private final BroadcastReceiver bondStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,BluetoothDevice.BOND_NONE);
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action) && bondState == BluetoothDevice.BOND_BONDED){
                // The bond state has changed to BOND_BONDED
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                returnBluetoothAddress(device);

            }

        }
    };

    private void returnBluetoothAddress(BluetoothDevice device){
        final String deviceAddress = device.getAddress();
        final String deviceName = device.getName();
        Bundle bundle = new Bundle();
        bundle.putString("device_name", deviceName);
        bundle.putString("device_address", deviceAddress);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_devices);

        getAllThePairedDevices();
    }

    private void getAllThePairedDevices() {
        // Show dialog if BluetoothAdapter is null
        if (mBluetoothAdapter == null) {

            // Create the dialog dialog box
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You device doesn't support bluetooth");
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.show();
        } else {

            // The device supports the Bluetooth device
            // If the bluetooth is off, go to BluetoothOff Activity
            if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                Intent intent = new Intent(this, BluetoothOffActivity.class);
                startActivityForResult(intent, ENABLE_BLUETOOTH);
            } else {
                // If the bluetooth is on, initialze the activity
                init();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ENABLE_BLUETOOTH) {
            if (resultCode == RESULT_OK) {
                init();
            }
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister the ACTION_FOUND receiver.
        unregisterReceiver(mReceiver);
        // Unregister the ACTION_BOND_STATE_CHANGED receiver.
        unregisterReceiver(bondStateChangeReceiver);

        super.onDestroy();
    }

    private void init() {
        //Get all the paired devices
        final Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesList.add(device);
            }
        }

        // Underline the text
        String pairedDevicesString = new String("Paired Devices");
        SpannableString contentPairedDevices = new SpannableString(pairedDevicesString);
        contentPairedDevices.setSpan(new UnderlineSpan(), 0, pairedDevicesString.length(), 0);

        tvPairedDevices = (TextView) findViewById(R.id.tvPairedDevices);
        tvPairedDevices.setText(contentPairedDevices);

        // Underline the text
        String availableDevicesString = new String("Available Devices");
        SpannableString contentAvailableDevices = new SpannableString(availableDevicesString);
        contentAvailableDevices.setSpan(new UnderlineSpan(), 0, availableDevicesString.length(), 0);

        tvAvailableDevices = (TextView) findViewById(R.id.tvAvailableDevices);
        tvAvailableDevices.setText(contentAvailableDevices);

        lvPairedDevices = (ListView) findViewById(R.id.lvPairedDevices);
        lvPairedDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                BluetoothDevice device = pairedDevicesList.get(position);
                //Return the bluetooth device to the main activity
                returnBluetoothAddress(device);
            }
        });
        lvPairedDevices.setAdapter(customPairedDevicesAdapter);

        lvAvailableDevices = (ListView) findViewById(R.id.lvAvailableDevices);
        lvAvailableDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = availableDevicesList.get(position);

                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    device.createBond();
                }
            }
        });
        lvAvailableDevices.setAdapter(customAvailableDevicesAdapter);

        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);

        // Register for broadcasts when a device is discovered
        IntentFilter filterDevice = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filterDevice);

        // Register for broadcasts when a device state is changed
        IntentFilter filterBond = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(bondStateChangeReceiver, filterBond);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnSearch.getId()) {
            if (!isScanning) {
                btnSearch.setText("Stop");
                isScanning = true;
                availableDevicesList.clear();
                customAvailableDevicesAdapter.notifyDataSetChanged();
                mBluetoothAdapter.startDiscovery();
            } else {
                btnSearch.setText("Search");
                isScanning = false;
                mBluetoothAdapter.cancelDiscovery();
            }
        }
    }


    class CustomPairedDevicesAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return pairedDevicesList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflate the bluetooth_device layout
            convertView = getLayoutInflater().inflate(R.layout.bluetooth_device, null);

            // Set the values to the view
            TextView tvBluetoothDeviceName = (TextView) convertView.findViewById(R.id.tvBluetoothDeviceName);
            TextView tvBluetoothDeviceAddress = (TextView) convertView.findViewById(R.id.tvBluetoothDeviceAddress);


            tvBluetoothDeviceName.setText(pairedDevicesList.get(position).getName());
            tvBluetoothDeviceAddress.setText(pairedDevicesList.get(position).getAddress());

            return convertView;
        }
    }

    class CustomAvailableDevicesAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return availableDevicesList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflate the bluetooth_device layout
            convertView = getLayoutInflater().inflate(R.layout.bluetooth_device, null);

            // Set the values to the view
            TextView tvBluetoothDeviceName = (TextView) convertView.findViewById(R.id.tvBluetoothDeviceName);
            TextView tvBluetoothDeviceAddress = (TextView) convertView.findViewById(R.id.tvBluetoothDeviceAddress);


            tvBluetoothDeviceName.setText(availableDevicesList.get(position).getName());
            tvBluetoothDeviceAddress.setText(availableDevicesList.get(position).getAddress());

            return convertView;
        }
    }
}
